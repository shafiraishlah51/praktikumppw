from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Status
from .forms import Status_Form
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest
import time

# Create your tests here.
class Story6UnitTest(TestCase):

    def test_hello_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_hello_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_template_hello(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'hello.html')

    def test_landing_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Hi!',html_response)
    
    def test_create_object_model (self):
        status_message = Status.objects.create(mystatus = 'ok')
        counting_object_status = Status.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_hello_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code,200)
    
    def test_hello_index_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, home)
    
    def test_template_hello(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_landing_page_(self):
        response = Client().get('/home/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Shafira Ishlah Nurulita',html_response)

    def test_form_validation_for_blank_items(self):
        form_invalid = Status_Form(data={'mystatus': ''})
        self.assertFalse(form_invalid.is_valid())
        self.assertEqual(
            form_invalid.errors['mystatus'],
            ["This field is required."]
        )

    def test_form_registration_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response = Client().post('/', {'mystatus': ''})
        self.assertEqual(response.status_code, 200)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

class VisitorTest (unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable') 
        chrome_options.add_argument('--no-sandbox') 
        chrome_options.add_argument('--headless') 
        chrome_options.add_argument('disable-gpu') 
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(VisitorTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    # Untuk Masukin coba coba di form status
    def test_input_Coba_Coba(self):
        self.browser.get('http://praktikumppw-shais.herokuapp.com/')
        time.sleep(5)
        search_box = self.browser.find_element_by_name('mystatus')
        search_box.send_keys('Coba Coba')
        search_box.submit()
        time.sleep(5)
        self.assertIn( "Coba Coba", self.browser.page_source)

    # Ngecek Judul 
    def test_title(self):
        self.browser.get('http://praktikumppw-shais.herokuapp.com/')
        time.sleep(5)
        self.assertIn("Shafira Ishlah Nurulita", self.browser.title)
    
    def test_title(self):
        self.browser.get('http://praktikumppw-shais.herokuapp.com/home/')
        time.sleep(5)
        self.assertIn("Shafira Ishlah Nurulita", self.browser.title)

    # Ngecek h1 isinya Status ga
    def test_header(self):
        self.browser.get('http://praktikumppw-shais.herokuapp.com/')
        header_text = self.browser.find_element_by_tag_name('h1').text
        time.sleep(5)
        self.assertIn("STATUS", header_text)

    def test_header(self):
        self.browser.get('http://praktikumppw-shais.herokuapp.com/home/')
        header_text = self.browser.find_element_by_tag_name('h1').text
        time.sleep(5)
        self.assertIn("HOME", header_text)
    
    # Ngecek text-align di CSS di center ga
    def test_header_with_css_property(self):
        self.browser.get('http://praktikumppw-shais.herokuapp.com/home/')
        header_text = self.browser.find_element_by_tag_name('h1').value_of_css_property('text-align')
        time.sleep(5)
        self.assertIn('center', header_text)
    
    # Ngecek Font-Sizenya ukuran 90px apa bukan
    def test_response_header_with_css_property(self):
        self.browser.get('http://praktikumppw-shais.herokuapp.com/home/')
        header_text = self.browser.find_element_by_tag_name('h1').value_of_css_property('font-size')
        time.sleep(5)
        self.assertIn('90px', header_text)
    